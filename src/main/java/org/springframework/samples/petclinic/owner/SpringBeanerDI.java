package org.springframework.samples.petclinic.owner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.*;

@Configuration
public class SpringBeanerDI {
	@Bean(name = "normalLogger")
	public Logger getLogger() {
		return LoggerFactory.getLogger("CRITICAL_LOGGER");
	}

	@Bean(name = "testLogger")
	public Logger getLoggerTest() {
		Logger logger = mock(Logger.class);
		return logger;
	}

	@Bean(name = "normalOwner")
	public Owner getOwner() {
		return new Owner();
	}

	@Bean(name = "testOwner")
	public Owner getOwnerTest() {
		return mock(Owner.class, CALLS_REAL_METHODS);
	}

	@Bean(name = "normalPet")
	public Pet getPet() {
		return new Pet();
	}

	@Bean(name = "testPet")
	public Pet getPetTest() {
		return mock(Pet.class, CALLS_REAL_METHODS);
	}
}
