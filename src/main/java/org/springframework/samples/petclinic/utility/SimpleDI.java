package org.springframework.samples.petclinic.utility;

import org.springframework.samples.petclinic.owner.PetRepository;

import java.util.HashMap;
import java.util.concurrent.Callable;

/**
 * this simple class shows the main idea behind a Dependency Injection library
 */
public class SimpleDI {

	private static SimpleDI simpleDI = null;
	private HashMap<Object, Object> hashMap;

	private SimpleDI() {
		hashMap = new HashMap<>();
	}

	public static SimpleDI getDIContainer() throws Exception {
		if (simpleDI == null) {
			simpleDI = new SimpleDI();
		}
		return simpleDI;
	}

	public void provideByInstance(Class<?> typeClass, Object instanceOfType) {
		hashMap.put(typeClass, instanceOfType);
	}

	public void provideByAConstructorFunction(Class<?> typeClass, Callable<Object> providerFunction) {
		hashMap.put(typeClass, providerFunction);
	}

	public Object getInstanceOf(Class<?> requiredType) throws Exception {
		Object obj = hashMap.get(requiredType);
		if(obj instanceof Callable) {
			return ((Callable<Object>) obj).call();
		}
		return obj;
	}
}
