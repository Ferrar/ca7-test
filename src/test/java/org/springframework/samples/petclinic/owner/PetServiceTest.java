package org.springframework.samples.petclinic.owner;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.samples.petclinic.utility.PetTimedCache;
import org.slf4j.Logger;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes=SpringBeanerDI.class)
class PetServiceTest {

	@Autowired
	@Qualifier("testOwner")
	private Owner ownerSample;

	private OwnerRepository ownerRepSample;

	@Autowired
	@Qualifier("testPet")
	private Pet petSample;

	private PetTimedCache petCacheSample;

	@Autowired
	@Qualifier("testLogger")
	private Logger loggerSample;

	private PetService petService;

	private void initializeUtility() {
		ownerSample.setId(1);
		petSample.setId(2);
	}
	/* For this test class, all test doubles are mock objects in code, but functionally,
	 Owner and Pet are used like a mock object, logger is used like a spy
	 and PetCache and OwnerRepository are used like stubs.

	 For all of these test cases, the Mockisty approach has been employed. Mocks are used mostly instead of real
	 objects, and we also have some amount of behavior verification.

	 PetTimedCache class utilizes and is depenent on so many other objects. It also uses threads that are hard to test
	 and also make unit testing with the classic approach fragile. This is why we should use mockisty approach to test
	 this class, and mock all these outer dependencies for steadier and more robust tests.
	 */

	@BeforeEach
	public void setUp() {
		initializeUtility();
		ownerRepSample = mock(OwnerRepository.class);
		petCacheSample = mock(PetTimedCache.class);
		petService = new PetService(petCacheSample, ownerRepSample, loggerSample);
	}

	@Test
	void testFindOwner() { // This test has both state verification with assert and behavior verification with verify.
		when(ownerRepSample.findById(1)).thenReturn(ownerSample);
		Owner foundOwner = petService.findOwner(1);
		assertEquals(1, foundOwner.getId());
		verify(loggerSample, atLeastOnce()).info(anyString(), anyInt());
	}

	@Test
	void testNewPet() { // This test has both state verification with assert and behavior verification with verify.
		Pet newPet = petService.newPet(ownerSample);
		assertEquals(ownerSample.getPets().size(), 1);
		verify(loggerSample, atLeastOnce()).info(anyString(), anyInt());
	}

	@Test
	void testFindPet() { // This test has both state verification with assert and behavior verification with verify.
		when(petCacheSample.get(2)).thenReturn(petSample);
		Pet foundPet = petService.findPet(2);
		assertEquals(foundPet.getId(), 2);
		verify(loggerSample, atLeastOnce()).info(anyString(), anyInt());
	}

	@Test
	void testSavePet() { // This test has both state verification with assert and behavior verification with verify.
		petService.savePet(petSample, ownerSample);
		assertEquals(petSample.getOwner().getId(), 1);
		verify(loggerSample, atLeastOnce()).info(anyString(), anyInt());
	}

	@AfterEach
	public void tearDown() {
		petService = null;
	}
}
