package org.springframework.samples.petclinic.utility;

import org.junit.jupiter.api.Test;
import org.springframework.samples.petclinic.owner.PetRepository;

import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class SimpleDITest {

	@Test // This test is a state verification, classical test.
	public void testProvideByInstance() throws Exception {
		SimpleDI diContainer = SimpleDI.getDIContainer();
		PetRepository petRepoMock = mock(PetRepository.class);
		// The test double used here is a Mock, but it's used as a Dummy Object.
		diContainer.provideByInstance(PetRepository.class, petRepoMock);
		PetRepository diProvidedRepo = (PetRepository) diContainer.getInstanceOf(PetRepository.class);
		assertEquals(petRepoMock, diProvidedRepo);
	}

	@Test // This test is a state verification, classical test.
	public void testProvideByFunction() throws Exception {
		SimpleDI simpleDi = SimpleDI.getDIContainer();
		simpleDi.provideByAConstructorFunction(PetRepository.class, new Callable<Object>() {
			@Override
			public Object call() throws Exception {
				return mock(PetRepository.class);
				// The test double used here is a Mock, but it's used as a Dummy Object.
			}
		});
		assertNotNull(simpleDi.getInstanceOf(PetRepository.class));
	}
}
